package com.llw.pushdemo;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

public class TestActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);

        TextView tvTest = findViewById(R.id.tv_test);

        String extras = getIntent().getStringExtra("extras");
        if(extras !=null){
            tvTest.setText(extras);
        }

    }
}
